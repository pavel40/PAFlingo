SLOVÍČKA:
mléko-milk
rýže-rice
noviny-newspaper
ženy-women
dívky-girls
muži-men
chlapci-boys
sendvič-sandwich
jíst-eat
pít-drink
číst-read
mít-have

GRAMATIKA:
Slovesa se 
v přítomném čase prostém 
většinou časují takto:
Číslo jednotné	Číslo množné
I eat                               we eat
you eat                        you eat
he (she, it) eatS      they eat