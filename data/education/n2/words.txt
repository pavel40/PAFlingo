SLOVÍČKA:
mléko-die Milch
rýže-der Reis
noviny-die Zeitung
ženy-Frauen
dívky-Mädchen
muži-Männer
chlapci-Jungen
sendvič-das Butterbrot

GRAMATIKA:
Před podstatnými jmény v množném čísle
se v neurčitém členu nedává žádný člen.
V případě určitého členu se dává člen "die".