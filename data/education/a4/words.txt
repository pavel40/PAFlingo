SLOVÍČKA:
jmenovat se-be called
pocházet z-be from, come from
mluvit-speak
rozumět-understand
Anglie-England
Německo-Germany
Kanada-Canada
Francie-France
Velká Británie-Great Britain
anglicky-English
německy-German

GRAMATIKA:
Časování sloves uvedených výše:
Časuje se pouze sloveso (např. I am called...)