SLOVÍČKA:
jídlo-food
hllad-hunger
pizza-pizza
polévka-soup
ryba-fish
zmrzlina-ice cream
dobrý-good
čaj-tea
žízeň-thirst
chutnat-taste
pivo-beer
víno-wine
ovoce-fruit
pomeranč-orange
banán-banana
šťáva-juice
jablečný džus-apple juice
pomerančový džus-orange juice
vejce-egg
sýr-cheese
zelenina-vegetables
brambora-potato
sladký-sweet
čokoláda-chocolate
cukr-sugar
čerstvý-fresh
chutný-tasty
sůl-salt
těstoviny-pasta
nudle-noodles
chutnat-taste