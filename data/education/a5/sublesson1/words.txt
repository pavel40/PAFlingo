SLOVÍČKA:
jídlo-food
hllad-hunger
pizza-pizza
polévka-soup
ryba-fish
led-ice
zmrzlina-ice cream
dobrý-good
čaj-tea
žízeň-thirst
chutnat-taste
káva-coffee
pivo-beer

GRAMATIKA:
Vyjádření		 podstatné jméno	       přídavné jméno
    hlad                       have hunger                           be hungry
   žízeň                       have thirst                              be thirsty