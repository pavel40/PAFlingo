žena-woman
dívka-girl
muž-man
chlapec-boy
dítě-child
jablko-apple
chléb-bread
voda-water
and-a

GRAMATIKA:
VÝSLOVNOST URČITÉHO ČLENU:
před souhláskou VE VÝSLOVNOSTI (např. the boy) ----- [d]
před samohláskou VE VÝSLOVNOSTI (např. the apple) ----- [dý]
Určitý člen THE píšeme, když myslíme něco určitého (např. the boy).
Neurčitý člen píšeme, když mluvíme o nějaké věci
nebo o ní mluvíme poprvé (např. a boy).
