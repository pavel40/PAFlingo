SLOVÍČKA:
zvíře-animal
žrát-eat
kočka-cat
pes-dog
myš-mouse
medvěd-bear

GRAMATIKA:
Mluvčí angličtiny nerozlišují "jíst" a "žrát".
Obojí říkají stejným slovem:"eat".