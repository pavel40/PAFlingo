SLOVÍČKA:
mít-haben

GRAMATIKA:
Časování nepravidelného 
slovesa "haben"(mít):
Číslo jednotné	Číslo množné
ich habe                      wir haben
du HAST                      ihr habt
er HAT                          sie haben

Skloňování určitého členu
v AKUSATIVU:
Rod mužský	Rod ženský
         den                       die
Rod střední      Množné číslo
          das                       die