SLOVÍČKA:
číst-lesen
kniha-das Buch
knihy-die Bücher

GRAMATIKA:
Časování nepravidelného
slovesa "lesen" (číst):
Číslo jednotné		Číslo množné
ich lese                                wir lesen
du LIEST                              ihr lest
er(sie, es) LIEST             sie lesen

Skloňování neurčitého
členu ("ein") v AKUSATIVU:
Rod mužský	Rod ženský
        einen                   eine
Rod stření		Množné číslo
       ein                              -